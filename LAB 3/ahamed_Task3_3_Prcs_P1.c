#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>

int main(int argc, char* argv[]) {
	int fd1, fd2;
	if(3 != argc) {
		printf("\nUsage: Not enoguh file destination name was given as argument! \n");
		printf("\nPlease give two valid filepath as command line argument to open\n\n");
		return 1;
	}

	errno = 0;
	fd1 = creat(argv[1],S_IRWXO|S_IRWXU|S_IRWXG);
	fd2 = creat(argv[2],S_IRWXO|S_IRWXU|S_IRWXG);
	if ( fd1 < 1 || fd2 < 1 ) {
		printf("Creat() failed with error [%s]\n", strerror(errno));
		return 1;
	}
	else {
		printf("%s, %s are Created successfully\n",argv[1],argv[2]);
		// closing the Opened file
		int close_fd1 = close(fd1);
		int close_fd2 = close(fd2);
		if ( close_fd1 < 0 || close_fd2) 
			printf("Close() failed with error [%s]\n",strerror(errno));
		else
			printf("Close() successful!\n");
	}

	return 0;
}

