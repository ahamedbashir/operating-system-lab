#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

int main(int argc, char *argv[]) {
	int status;
	pid_t child = fork();
	if(child == -1) {
		perror("FORK:");
	}
	else if ( child == 0 ) {
		printf("Forked Success.\nI am the child, my PID: %d\n",getpid());
		execl("/bin/date","Current Date and Time", NULL);
		perror("EXECL Failed\n");
	}
	else {
		pid_t pid = waitpid(child, &status, 0);	// Checks Termination status of Process Q
		if (pid == -1) {
			printf("Termination failed with status: %d",status);
			exit(EXIT_FAILURE);
		}
	}

	return 0;
}
