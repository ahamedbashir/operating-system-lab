/*
 Author: Bashir Ahamed
 CSC 322, Spring 2018
 CCNY, Computer  Science,
*/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>


int main() {

    int status;
	pid_t pid;
	char *arg[] = {"./Prcs_P1","destination1.txt","destination2.txt",NULL};
	// create the first process by calling fork() on the current process
        pid_t process_one = fork();
		
        if ( process_one < 0 ) {	// checks fork failure error
            perror("Fork:");
        }
        else if(process_one == 0) {	// process Process One is running
		printf("\n****Process One, pid: %d , parent pid: %d ****\n", getpid(),getppid());
		sleep(1);
		execv("./Prcs_P1",arg);
		perror("EXECV Failed to execute Prcs_P1\n");
        }
        else  {
		 do {				// calls waitpid() to make sure the termination of Process one before the parent process to call execute any other process
			pid = waitpid(process_one, &status, WUNTRACED | WCONTINUED);	// Checks Termination status of Process one
			if (pid == -1) {
				perror("waitpid");
				exit(EXIT_FAILURE);
			}
			if (WIFEXITED(status)) {
				printf("\tPrcoess One executed normally!, Status=%d\n", WEXITSTATUS(status));
			} else if (WIFSIGNALED(status)) {
				printf("\tProcess One killed by signal %d\n", WTERMSIG(status));
			} else if (WIFSTOPPED(status)) {
				printf("\tPrcoess One stopped by signal %d\n", WSTOPSIG(status));
			} else if (WIFCONTINUED(status)) {
				printf("\tProcess One continued\n");
			}
		} while (!WIFEXITED(status) && !WIFSIGNALED(status));
                
		pid_t process_two = fork();  // create the Second prcoess by calling fork() on the current process after executing process One
                if (process_two < 0 ) {		// checks fork failure error
                        perror("Fork:");
                }
                else if ( process_two == 0 ) {		// process Process Two is running
			char *arg2[] = {"./Prcs_P2","source.txt",arg[1],arg[2],NULL};   // elements of array arg is copied to make sure no mistake is done for the command argument parameter
			printf("\n****Process two, pid: %d , parent pid: %d ****\n", getpid(),getppid());
			sleep(1);
			execv(arg2[0],arg2);
			perror("EXECV Failed to execute process Prcs_P2\n");
			
		}
        else {
			do {		// Checks Termination status of Process one
				pid = waitpid(process_two, &status, WUNTRACED | WCONTINUED);	// Checks Termination status of Process one
				if (pid == -1) {
					perror("waitpid");
					exit(EXIT_FAILURE);
				}
				if (WIFEXITED(status)) {
					printf("\tProcess Two Executed normally, Status=%d\n", WEXITSTATUS(status));
					} 
				else if (WIFSIGNALED(status)) {
					printf("\tProcess Two killed by signal %d\n", WTERMSIG(status));
				}
				else if (WIFSTOPPED(status)) {
					printf("\tProcess Two stopped by signal %d\n", WSTOPSIG(status));
				} else if (WIFCONTINUED(status)) {
					printf("\tProcess Two continued\n");
				}
			} while (!WIFEXITED(status) && !WIFSIGNALED(status));
		}
	}
	sleep(1);
	printf("\n");
        return 0;
}
