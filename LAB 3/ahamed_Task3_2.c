#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

int main(int argc, char *argv[]) {
	int status;
	char *arg[] = {"ls", "-la","/bin",NULL};
	pid_t child = fork();
	if(child == -1) {
		perror("FORK:");
	}
	else if ( child == 0 ) {
		printf("Forked Success.\nI am the child, my PID: %d\n\n",getpid());
		execvp(arg[0],arg);
		perror("EXECVP Failed\n");
	}
	else {
		pid_t pid = waitpid(child, &status, 0);	// Checks Termination status of Process Q
		if (pid == -1) {
			printf("Termination failed with status: %d",status);
			exit(EXIT_FAILURE);
		}
	}

	return 0;
}
