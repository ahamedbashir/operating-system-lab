#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>

int main() {
	pid_t worker1, worker2;
	pid_t cpid1, cpid2;
	int status;

	double grade[10][10];
	double avg;
	const int total_student = 10;
	const int total_chapter = 5;
	const int hw_per_chapter = 2;
	FILE * fd = fopen("class_grades.txt","r");
	int temp;	
	for ( int i =0; i < total_student; ++i ) {
		for ( int j = 0; j < total_chapter * hw_per_chapter; ++j) {
			fscanf(fd, "%d", &temp);
			if ( temp != EOF)
				grade[i][j] = temp;
		}
	}
	pid_t manager[5];
	for( int i = 0; i < 5; ++ i ) {
		printf("%d \n" ,getppid());
		switch(manager[i] = fork() ) {
			case -1:
				{
					perror("Forked:");
					exit(1);
				}

			case 0:
				{
					printf("Manager %d with PID: %d, Parent PID: %d, processing: \n", i+1, getpid(), getppid());
					int HW1 = (i)*2;
					int HW2 = HW1 + 1;

					if (( worker1 = fork()) ==  0) {
						double total_grade = 0;
						for( int j = 0; j < 10; ++j ) {
							total_grade += grade[j][HW1];
						}
						avg = total_grade/10;
						printf("Chapter: %d, HW: 01 \n", i+1);
						printf("Total Grade = %.2f, Average Grade = %.2f \n", total_grade, avg);
						printf("\tWorker1 with PID: %d returned to parent manager with PID: %d \n", getpid(), getppid());
						sleep(1);
						exit(0);
					}
					else if ( (worker2 = fork()) == 0 ) {
						double total_grade = 0;
						for( int j = 0; j < 10; ++j ) {
							total_grade += grade[j][HW2];
						}
						avg = total_grade/10;
						printf("Chapter: %d, HW: 02 \n", i+1);
						printf("Total Grade = %.2f, Average Grade = %.2f \n", total_grade, avg);
						printf("\tWorker2 with PID: %d returned to parent manager with PID: %d \n", getpid(), getppid());
						sleep(1);
						exit(0);

					}

					else if ( worker1 < 0) {
						perror("Forked:");
					}
					else if ( worker2 < 0 ) {
						perror("Forked:");
					}
					else {
						if( (cpid1 = wait(&status)) == worker1 ) {
							//printf("\tWorker1 with PID: %d returned to parent manager with PID: %d \n", worker1, getppid());
						}
						 if( (cpid2 = wait(&status)) == worker2 ) {
							//printf("\tWorker2 with PID: %d returned to parent manager with PID: %d \n", worker2, getppid());
						}
						
					}
					exit(0);

				}
			default:
				sleep(1);
				printf("Director with PID: %d, has processed manager %d with PID: %d \n", getpid(), i+1, manager[i]);
		}
	}

	return 0;
					
}

