#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>

int main() {
	const int total_student = 10;
	const int total_chapter = 5;
	const int hw_per_chapter = 2;
	int total_hw = total_chapter*hw_per_chapter;
	int status;

	double grade[total_student][total_chapter];
	double avg;
	FILE * fd = fopen("class_grades.txt","r");
	int temp;	
	for ( int i =0; i < total_student; ++i ) {
		for ( int j = 0; j < total_hw; ++j) {
			fscanf(fd, "%d", &temp);
			if ( temp != EOF)
				grade[i][j] = temp;
		}
	}
	pid_t manager[total_chapter];
	for( int i = 0; i < total_chapter; ++ i ) {
		switch(manager[i] = fork() ) {
			case -1:
				{
					perror("Forked:");
					exit(1);
				}

			case 0:
				{	printf("Manager %d with PID: %d, Parent PID: %d, processing: \n", i+1, getpid(), getppid());
					pid_t worker[total_hw];
					for ( int j = 0; j < hw_per_chapter; ++j) {
						int hw = i*hw_per_chapter + j;
						if (( worker[j] = fork()) ==  0) {
							printf("\tWorker: %d with PID: %d parent manager with PID: %d is processing:\n",j+1, getpid(), getppid());
							double total_grade = 0;
							for( int std = 0; std < total_student; ++std ) {
								total_grade += grade[std][hw];
							}
							avg = total_grade/10;
							printf("\t\tChapter: %d, HW: %d \n", i+1, j+1);
							printf("\t\tTotal Grade = %.2f, Average Grade = %.2f \n\n", total_grade, avg);
							
							//sleep(1);
							exit(0);
						}

						else if ( worker < 0) {
							perror("Forked:");
						}
						else {
							if( wait(&status) == worker[j] ) {
								
							}
						}
					}
					exit(0);
					

				}
			default:
				if ( wait(&status) == manager[i]) {
					sleep(1);
					printf("\tDirector with PID: %d, has processed manager %d with PID: %d \n\n\n", getpid(), i+1, manager[i]);
				}
		}

	}

	printf("**** Director with PID: %d processed all the manager ****\n\n", getpid());

	return 0;
					
}

