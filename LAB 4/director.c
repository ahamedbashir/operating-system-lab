#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>

int main() {
	const int total_student = 10;
	const int total_chapter = 5;
	const int hw_per_chapter = 2;
	int total_hw = total_chapter*hw_per_chapter;
	int status;

	double grade[total_student][total_chapter];
	double avg;
	FILE * input_file;
	FILE * output_file;
	FILE * log_file;
	// checks log file opening success
	if((log_file = fopen("log_file.txt", "w+")) == NULL){
		perror("Open Failed:");
		return -1;
	}
	// opens input file
	if ( (input_file = fopen("class_grades.txt","r")) == NULL){
		perror("Open Failed:");
		return -1;
	}
	// opens output file
	if ((output_file = fopen("class_average.txt", "w+")) == NULL){
		perror("Open Failed:");
		return -1;
	}
	// reads class grades from input file
	for ( int i =0; i < total_student; ++i ) {
		for ( int j = 0; j < total_hw; ++j) {
			int temp;
			fscanf(input_file, "%d", &temp);
			if ( temp != EOF)
				grade[i][j] = temp;
		}
	}
	// managers
	pid_t manager[total_chapter];
	for( int i = 0; i < total_chapter; ++ i ) {
		switch(manager[i] = fork() ) {   // forks manager
			case -1:
				{
					perror("Forked:");
					fprintf(log_file, "%s%d\n","Forked failed for manager: ",i+1);
					exit(1);
				}

			case 0:
				{	printf("Manager %d with PID: %d, Parent PID: %d, processing: \n", i+1, getpid(), getppid());
					fprintf(log_file,"%s%d%s%d%s%d%s\n","Manager ",i+1," with PID: ",getpid()," Parent PID: ",getppid()," processing:");
					pid_t worker[total_hw];
					for ( int j = 0; j < hw_per_chapter; ++j) {
						int hw = i*hw_per_chapter + j;
						if (( worker[j] = fork()) ==  0) {  // forkes worker process
							printf("\tWorker: %d with PID: %d parent manager with PID: %d is processing:\n",j+1, getpid(), getppid());
							double total_grade = 0;
							for( int std = 0; std < total_student; ++std ) {
								total_grade += grade[std][hw];
							}
							avg = total_grade/10;
							printf("\t\tChapter: %d, HW: %d \n", i+1, j+1);
							fprintf(log_file, "%s%d%s%d\n","\t\tChapter: ",i+1, " HW: " , j+1);
							printf("\t\tTotal Grade = %.2f, Average Grade = %.2f \n\n", total_grade, avg);
							fprintf(log_file, "%s%.2f%s%.2f\n\n", "\t\tTotal Grade = ", total_grade," Average Grade = ", avg );
							fprintf(output_file, "%s%d%s%d%s%.2f\n", "Chapter: ",i+1, " HW: ",j+1, " Average = ", avg);
							exit(0);
						}

						else if ( worker < 0) {
							perror("Forked:");
							fprintf(log_file, "%s%d\n","Forked failed for worker: ",j+1);
						}
						else {
							do {		// Checks Termination status of workers
								int pid = waitpid(worker[j], &status, WUNTRACED | WCONTINUED);	// Checks Termination status of Child one
								if (pid == -1) {
									perror("waitpid");
									exit(EXIT_FAILURE);
								}
								if (WIFEXITED(status)) {
									printf("\tWorker terminated normally, Status=%d\n", WEXITSTATUS(status));
									} 
								else if (WIFSIGNALED(status)) {
									printf("\tWorker killed by signal %d\n", WTERMSIG(status));
								}
								else if (WIFSTOPPED(status)) {
									printf("\tWorker stopped by signal %d\n", WSTOPSIG(status));
								} else if (WIFCONTINUED(status)) {
									printf("\tWorker continued\n");
								}
							} while (!WIFEXITED(status) && !WIFSIGNALED(status));
						}
					}
					exit(0);


				}
			default:
				if ( wait(&status) == manager[i]) {
					sleep(1);
					printf("\tDirector with PID: %d, has processed manager %d with PID: %d \n\n\n", getpid(), i+1, manager[i]);
					fprintf(log_file, "%s%d%s%d%s%d\n\n\n", "\tDirector with PID: ",getpid(),", has processed manager ",i+1," with PID: ",manager[i]);
				}
		}

	}

	fprintf(log_file, "%s%d%s\n", "**** Director with PID: ", getpid()," processed all the managers ****");

	if ( fclose(input_file)|| fclose(output_file)||fclose(log_file) ) {
		printf("%s\n","Close Failed" );
		fprintf(log_file, "%s\n", "Close Failed");
		return -1;
	}
	else {
		printf("Closing files success!\n");
		printf("**** Director with PID: %d processed all the managers ****\n\n", getpid());
	}

	return 0;

}
