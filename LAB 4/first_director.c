#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>

//double grades[10][10];
//double avg[10];
void manage(const double grade[10][10], double *avg, int chapter) {
	pid_t worker1, worker2;
	pid_t cpid1, cpid2;
	int status;
	int HW1 = (chapter-1)*2;
	int HW2 = HW1 + 1;
	// print grades
	for ( int i = 0; i < 10; ++i) {
		for ( int j = 0; j < 10; ++j)
			printf("%d ", grade[i][j]);
		printf("\n");
	}
	if (( worker1 = fork()) ==  0) {
		double total_grade = 0.00;
		for( int i = 0; i < 10; ++i ) {
			total_grade += grade[i][HW1];
		}
		avg[HW1] = total_grade/10;
		sleep(2);
		exit(0);
	}
	else if ( (worker2 = fork()) == 0 ) {
		double total_grade = 0.00;
                for( int i = 0; i < 10; ++i ) {
                        total_grade += grade[i][HW2];
                }
                avg[HW2] = total_grade/10;

                sleep(2);
                exit(0);

	}

	else if ( worker1 < 0) {
		perror("Forked:");
	}
	else if ( worker2 < 0 ) {
		perror("Forked:");
	}
	else {
		if( (cpid1 = wait(&status)) == worker1 ) {
			printf("Worker1 with PID: %d returned\n", worker1);
		}
		 if( (cpid2 = wait(&status)) == worker2 ) {
                        printf("Worker2 with PID: %d returned\n", worker2);
                }
		
		printf("Chapter: %d, HW:1, Average = %d \n", chapter, avg[HW1]);
		printf("Chapter: %d, HW:2, Average = %d \n", chapter, avg[HW2]);
	}
}
int main() {
	double grades[10][10] = { {1,2,3,4,5,6,7,8,9,10},{1,2,3,4,5,6,7,8,9,10},{1,2,3,4,5,6,7,8,9,10},
		{1,2,3,4,5,6,7,8,9,10},{1,2,3,4,5,6,7,8,9,10},{1,2,3,4,5,6,7,8,9,10},
		{1,2,3,4,5,6,7,8,9,10},{1,2,3,4,5,6,7,8,9,10},{1,2,3,4,5,6,7,8,9,10},
		{1,2,3,4,5,6,7,8,9,10}};
	static double avg[10];
	pid_t manager[5];

	for( int i = 0; i < 5; ++ i ) {
		switch(manager[i] = fork() ) {
			case -1:
				{
					perror("Forked:");
					exit(1);
				}

			case 0:
				{
					manage(grades, avg, i+1);
					exit(0);

				}
			default:
				sleep(2);
				printf("Manager %d with PID: %d completed processing\n", i+1, manager[i]);
		}

	}

	exit(0);

	return 0;
					
}

