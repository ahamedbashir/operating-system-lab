// Author: Bashir Ahamed
// CCNY, Spring 2018

#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>

int main(int argc, char* argv[]) {
	int fd;
	if(2 != argc) {
		printf("\nUsage: No filepath was given as argument! \n");
		printf("\nPlease give a valid filepath as command line argument\n\nto Display the contents of that file\n\n");
		return 1;
	}

	errno = 0;
	
	// opening the source destination filepath to read from in Read only mode
	fd = open (argv[1], O_RDONLY);
	if ( fd < 0 ) {
		printf("\nCouldn't open the defined file\n");
		perror("open:"); // prints the error messages
		return 1;
	}
	else {
		int check;
		char buffer[10000];
		while( check = read(fd,buffer,1) >0) // check variable value verifies the successfulness of reading the file
			printf("%s", buffer);
		if ( check < 0 ) 
			perror("Read:");
		else
			printf("\nContents of %s Read successful\n", argv[1]);
		
		// closing the opened file after read() is completed
		check = close(fd);		
		if ( check < 0) {
			perror("close:");
			return 1;
		}
		else
			printf("%s closed successfully\n", argv[1]); 
					
	}

	return 0;
}
