// Author: Bashir Ahamed
// CCNY, Spring 2018

#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>

int main(int argc, char* argv[]) {
	int fd;
	if(2 != argc) {
		printf("\nUsage: No file destination was given as argument! \n");
		printf("\nPlease give a valid filepath as command line argument to open\n\n");
		return 1;
	}

	errno = 0;
	
	// opening filepath, or creating a new file
	fd = open (argv[1], O_RDONLY|O_CREAT);
	if ( fd < 1 ) {
		printf("\nOpen() failed with error [%s]\n", strerror(errno));
		return 1;
	}
	else {
		printf("\nOpen() or Creat() successful\n");
		
		// closing the Opened file
		int close_fd = close(fd);
		if ( close_fd < 0)
			printf("Close() failed with error [%s]\n",strerror(errno));
		else
			printf("Close() successful!\n");
	}

	return 0;
}
