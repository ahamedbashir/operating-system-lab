// Author: Bashir Ahamed
// CCNY, Spring 2018

#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>

int main(int argc, char* argv[]) {
	int input_fd, output_fd;

	// check validity of the argument variables
	if(3 != argc) {
		printf("\nUsage: Not enough filepath was given as argument! \n");
		printf("\nPlease give valid filepath as command line argument\n\nFirst Argument is the filepath to be read\n");
		printf("\nsecond argument is the filepath to write\n\n");
		return 1;
	}

	errno = 0;
	// opening the input filepath in read only mode
	input_fd = open (argv[1], O_RDONLY); // opens source file to copy
	// checking the whether the open() system call completed
	if ( input_fd < 0 ) {
		printf("\nCouldn't open the defined file: %s\n", argv[1]);
		perror("open:");
		return 1;
	}
	else {
		output_fd = creat (argv[2], O_WRONLY); // creates a writable file only when the input file is opened successfully
		if ( output_fd < 0 ) {
			printf("\nCouldn't create the defined file: %s\n", argv[2]);
			perror("Create:");
			// closing the currently opened files as the writable file creation failed
			input_fd = close(input_fd);
			if ( input_fd < 0 ) {
				printf("\n%s closed failed!\n",argv[1]);
				perror("Error: ");
			}
			else 
				printf("\n%s closed successfully!\n",argv[1]);
			
			return 1;
		}
		else {
			int read_fd, write_fd1, write_fd2;
			char buffer[100];
			while( (read_fd = read(input_fd, buffer, 100)) > 0 ) {
				for(int i = 0; i < (ssize_t)read_fd; ++i) {
					if (buffer[i] == '1') 
						buffer[i] = 'A';
				}
				write_fd1 = write(output_fd, buffer, (ssize_t)read_fd);
				write_fd2 = write(output_fd,"XYZ",3);
			}
			// checks read() and write() system calls completion
			if ( read_fd < 0 || write_fd1 < 0 || write_fd2 < 0) {
				if ( read_fd < 0)
					printf("\nContents of %s read unsuccessful\n", argv[1]);
				if ( write_fd1 < 0)
					printf("\nContents from %s couldn't be write successfully to %s\n", argv[1], argv[2]);
				if ( write_fd2 < 0 )
					printf("\nInsertion of XYZ in file: %s after every 100 characters failed!\n", argv[2]);
			}
			else
				printf("\nOpen() and write() was successful with Transliteration of 1 by A\n\nand insertion of XYZ in file: %s after every 100 characters\n", argv[2]);
			
			// closing files
			input_fd = close(input_fd);
			output_fd = close(output_fd);
			if ( input_fd < 0 ) {
				printf("\n%s closed failed!\n",argv[1]);
				perror("Error: ");
			}
			else 
				printf("\n%s closed successfully!\n",argv[1]);
			if ( output_fd < 0 ) {
				printf("\n%s closed failed!\n",argv[2]);
				perror("Error: ");
			}
			else 
				printf("\n%s closed successfully!\n\n",argv[2]);

		}
	}

	return 0;
}
