// Author: Bashir Ahamed
// CCNY, Spring 2018

#include <stdio.h>
#include <unistd.h>
#include <errno.h>

int main( int argc, char* argv[]) {
	char* filepath =  argv[1];
	int return_value;

	if(2 != argc) {
		printf("\nUsage: No file destination was given as argument! \n");
		printf("\nPlease give a valid filepath as command line argument to check persmission\n\n");
		return 1;
	}
	// check the existence of the named file
	return_value = access (filepath, F_OK);
	if (return_value == 0) {
		printf("\n%s exists!\n", filepath);
	}
	else {
		if ( errno == ENOENT) {
			printf ("%s doesn't exist!\n", filepath);
		}
		else if ( errno == EACCES) {
			printf ( "%s is not accessible!\n", filepath);
		}
		return 0;
	}

	// checks read access
	
	return_value = access(filepath, R_OK);
	if ( return_value == 0) {
		printf("%s is readable!\n",filepath);
	}
	else {
		if ( errno == ENOENT) {
			printf ("%s doesn't exist!\n", filepath);
		}
		else if ( errno == EACCES) {
			printf ( "%s is not accessible!\n", filepath);

		}
		else 
			printf("%s is not readable!\n",filepath);
		return 0;
	}

	// checks write access
	return_value = access(filepath, W_OK);
	if ( return_value == 0 ) {
		printf("%s is writeable\n", filepath);
	}
	 else {
                if ( errno == ENOENT) {
                        printf ("%s doesn't exist!\n", filepath);
                }
                else if ( errno == EACCES) {
                        printf ( "%s is not accessible!\n", filepath);

                }
                else
                        printf("%s is writeable\n",filepath);
                return 0;
        }
	
	// checks excutable access
	return_value = access ( filepath, X_OK);
		if( return_value == 0 ) {
			printf("%s is executable!\n",filepath);        
	       	}
	else {
                if ( errno == ENOENT) {
                        printf ("%s doesn't exist!\n", filepath);
                }
                else if ( errno == EACCES) {
                        printf ( "%s is not accessible!\n", filepath);

                }
                else
                        printf("%s is not executable\n",filepath);
                return 0;
        }
}
