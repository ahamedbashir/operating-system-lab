/*
 Author: Bashir Ahamed
 CSC 322, Spring 2018
 CCNY, Computer  Science,
*/
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>

int main() {
	int status;
	pid_t pid;
	
	int a = 10, b = 25, fq = 0, fr = 0;

	fq = fork();
  	if ( fq < 0 ) {	// checks fork failure error
                perror("Fork:");
        }
	else if(fq == 0 ) {
		a = a+b;
		printf("\n The value of a = %d,\tb = %d,\tPID = %d,\tProcessed by Q\n",a,b,getpid());

		
		fr = fork();

                if (fr < 0 ) {		// checks fork failure error
                        perror("Fork:");
                }
		else if ( fr != 0 ) {
			b = b + 20;
			printf("\n The value of a = %d,\tb = %d,\tPID = %d,\tProcessed by Q\n",a,b,getpid());
			
			// calls waitpid() to make sure the termination of child one before the parent process
			pid = waitpid(fr, &status, 0);	// Checks Termination status of Process Q
			if (pid == -1) {
				printf("Termination failed with status: %d",status);
				exit(EXIT_FAILURE);
			}
		}
		else {
			a = (a*b) + 30;
			printf("\n The value of a = %d,\tb = %d,\tPID = %d,\tPrcoessed by R\n",a,b,getpid());
		}
	}
	else {
		
		b = a + b - 5;
		printf("\n The value of a = %d,\tb = %d,\tPID = %d,\tProcessed by P\n",a,b,getpid());
		
		// calls waitpid() to make sure the termination of child one before the parent process

		pid = waitpid(fq, &status, 0);	// Checks Termination status of Process Q
		if (pid == -1) {
			printf("Termination failed with status: %d",status);
			exit(EXIT_FAILURE);
		}
	}
	return 0;
}
