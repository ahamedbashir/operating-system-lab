/*
 Author: Bashir Ahamed
 CSC 322, Spring 2018
 CCNY, Computer  Science,
*/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>


int main() {

        int status;
	pid_t pid;
	
	// create the first child by calling fork() on the current process
        pid_t child_one = fork();
		
        if ( child_one < 0 ) {	// checks fork failure error
                perror("Fork:");
        }
        else if(child_one == 0) {	// process Child One is running
                printf("\nI am child one, my pid is: %d and my parent pid is: %d\n", getpid(),getppid()); 
        }
        else  {
		 do {				// calls waitpid() to make sure the termination of child one before the parent process
			pid = waitpid(child_one, &status, WUNTRACED | WCONTINUED);	// Checks Termination status of Child one
			if (pid == -1) {
				perror("waitpid");
				exit(EXIT_FAILURE);
			}
			if (WIFEXITED(status)) {
				printf("\tChild One terminated normally!, Status=%d\n", WEXITSTATUS(status));
			} else if (WIFSIGNALED(status)) {
				printf("\tChild One killed by signal %d\n", WTERMSIG(status));
			} else if (WIFSTOPPED(status)) {
				printf("\tChild One stopped by signal %d\n", WSTOPSIG(status));
			} else if (WIFCONTINUED(status)) {
				printf("\tChild One continued\n");
			}
		} while (!WIFEXITED(status) && !WIFSIGNALED(status));
                
		pid_t child_two = fork();  // create the Second child by calling fork() on the current process which is parent as the first child is terminated already
                if (child_two < 0 ) {		// checks fork failure error
                        perror("Fork:");
                }
                else if ( child_two == 0 ) {		// process Child Two is running
                        printf("\nI am child two, my pid is: %d and my parent pid is: %d\n", getpid(),getppid());
		}
                else {
			do {		// Checks Termination status of Child one
				pid = waitpid(child_two, &status, WUNTRACED | WCONTINUED);	// Checks Termination status of Child one
				if (pid == -1) {
					perror("waitpid");
					exit(EXIT_FAILURE);
				}
				if (WIFEXITED(status)) {
					printf("\tChild Two terminated normally, Status=%d\n", WEXITSTATUS(status));
					} 
				else if (WIFSIGNALED(status)) {
					printf("\tChild Two killed by signal %d\n", WTERMSIG(status));
				}
				else if (WIFSTOPPED(status)) {
					printf("\tChild Two stopped by signal %d\n", WSTOPSIG(status));
				} else if (WIFCONTINUED(status)) {
					printf("\tChild Two continued\n");
				}
			} while (!WIFEXITED(status) && !WIFSIGNALED(status));
		}
        }
	printf("\n");
        return 0;
}
